package com.mycomp.pager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class PagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagerApplication.class, args);
	}

}
