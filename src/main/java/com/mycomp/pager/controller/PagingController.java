package com.mycomp.pager.controller;

import com.mycomp.pager.service.PagingService;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/LineBreak")
public class PagingController {

    @Autowired
    private PagingService pagingService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public UUID doPaging(@RequestBody String rawText) {
        return pagingService.doPaging(rawText);
    }
    
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public HttpStatus getPager(@PathVariable UUID id) {
        return pagingService.getPager(id);
    }
}
