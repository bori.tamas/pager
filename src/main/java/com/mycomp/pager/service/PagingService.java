package com.mycomp.pager.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class PagingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PagingService.class);
    private static final String DELIMITER = " ";

    /** The list of pager threads */
    private final Map<UUID, Thread> threads = new HashMap<>();

    /** Maximum linesize defined in application.properties */
    @Value("${pager.maxlinesize}")
    private int lineSize;

    public UUID doPaging(String rawText) {
        UUID threadId = UUID.randomUUID();
        PagerThread pt = new PagerThread(threadId, rawText);
        threads.put(threadId, pt);
        pt.start();
        
        return threadId;
    }

    public HttpStatus getPager(UUID id) {
        Thread th = threads.get(id);
        if (th != null && th.getState().equals(Thread.State.TIMED_WAITING)) {
            return HttpStatus.ACCEPTED;
        } else {
            return HttpStatus.NOT_FOUND;
        }
    }

    public class PagerThread extends Thread {

        private final UUID threadId;
        private final String rawText;

        public PagerThread(UUID threadId, String rawText) {
            this.threadId = threadId;
            this.rawText = rawText;
        }

        @Override
        public void run() {
            StringTokenizer tokens = new StringTokenizer(rawText, DELIMITER);
            List<String> lines = new ArrayList<>();
            lines.add("");
            int i = 0;
            while (tokens.hasMoreTokens()) {
                String word = tokens.nextToken().trim();
                if (lines.get(i).concat(word).concat(" ").length() > lineSize) {
                    lines.add("");
                    i++;
                }
                lines.set(i, lines.get(i).concat(word + " "));
                System.out.println(this.threadId + " " + word);
                try {
                    sleep(3000);
                } catch (InterruptedException ex) {
                    LOGGER.error("Pager Thread with id " + threadId + " was suddenly interrupted!");
                }
            }
        }
    }
}
